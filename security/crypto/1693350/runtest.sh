#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1
. ../../../cki_lib/libcki.sh

rlJournalStart
    rlPhaseStartSetup
        rlShowRunningKernel
        if ! cki_is_kernel_automotive; then
            yum install libkcapi-tools \
                    -y --enablerepo=*
        fi
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "which kcapi-enc"
        rlRun "for i in {1..1000}; do head -c $((16*1024)) /dev/zero | kcapi-enc -e -c 'ctr(aes)' --passwdfd test -s test --pbkdfiter 1 | sha256sum; done | sort -u | tee output.log"
        rlAssertEquals "all output are the same" $(wc -l output.log | awk '{print $1}') 1
    rlPhaseEnd

    rlPhaseStartCleanup
    rlPhaseEnd
rlJournalEnd
rlJournalPrintText
